﻿using DigitalSalmon.C360.AnimatedComponents;
using UnityEngine;

namespace DigitalSalmon.C360 {
	[ExecuteInEditMode]
	[AddComponentMenu("Complete 360 Tour/Examples/Popup")]
	public class Popup : AnimatedHotspot {
		//-----------------------------------------------------------------------------------------
		// Inspector Variables:
		//-----------------------------------------------------------------------------------------

		[Header("Developer")]
		[SerializeField]
		protected bool showTransitionAlpha = false;

		[SerializeField]
		protected float transitionAlpha;

		//-----------------------------------------------------------------------------------------
		// Private Fields:
		//-----------------------------------------------------------------------------------------

		private IAnimatedComponent[] animatedComponents;

		//-----------------------------------------------------------------------------------------
		// Public Properties:
		//-----------------------------------------------------------------------------------------

		public override bool IsValid => true;

		//-----------------------------------------------------------------------------------------
		// Unity Lifecycle:
		//-----------------------------------------------------------------------------------------

		protected override void Awake() {
			base.Awake();
			animatedComponents = GetComponentsInChildren<IAnimatedComponent>(true);
			SetVisible(true);
			SetInteractive(true);
		}

		protected virtual void Start() { OnHoveredDeltaUpdate(0); }

		protected void Update() {
			if (showTransitionAlpha && !Application.isPlaying) {
				if (animatedComponents == null) animatedComponents = GetComponentsInChildren<IAnimatedComponent>();
				transitionAlpha = Mathf.Clamp01(transitionAlpha);
				OnHoveredDeltaUpdate(transitionAlpha);
			}
		}

		//-----------------------------------------------------------------------------------------
		// Protected Methods:
		//-----------------------------------------------------------------------------------------

		protected override void OnHoveredDeltaUpdate(float alpha) {
			base.OnHoveredDeltaUpdate(alpha);
			float delta = Easing.QuadEaseInOut(alpha);

			foreach (IAnimatedComponent animatedComponent in animatedComponents) {
				animatedComponent.OnAlphaChanged(delta);
			}
		}
	}
}