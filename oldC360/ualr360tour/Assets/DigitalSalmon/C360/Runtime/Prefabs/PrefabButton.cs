﻿using System.Collections;
using UnityEngine;
using TMPro;

namespace DigitalSalmon.C360 {
	public class PrefabButton : BaseBehaviour {

		[SerializeField]
		protected string text;

		//-----------------------------------------------------------------------------------------
		// Private Fields:
		//-----------------------------------------------------------------------------------------

		private TMP_Text textComponent;
		private BoxCollider boxCollider;
		private Popup popup;

		//-----------------------------------------------------------------------------------------
		// Unity Lifecycle:
		//-----------------------------------------------------------------------------------------

		protected void Awake() { LocateComponents(); }

		protected void OnEnable() { popup.Triggered += Popup_Triggered; }

		protected void OnValidate() {
			LocateComponents();
			Initialise();
		}

		protected IEnumerator Start() {
			yield return new WaitForEndOfFrame();
			Initialise();
		}

		protected void OnDisable() { popup.Triggered += Popup_Triggered; }

		//-----------------------------------------------------------------------------------------
		// Event Handlers:
		//-----------------------------------------------------------------------------------------
		
		protected virtual void Popup_Triggered(Hotspot value) { }

		//-----------------------------------------------------------------------------------------
		// Private Methods:
		//-----------------------------------------------------------------------------------------

		protected virtual void LocateComponents() {
			popup = GetComponent<Popup>();
			textComponent = GetComponentInChildren<TMP_Text>();
			boxCollider = GetComponent<BoxCollider>();
		}

		protected virtual void Initialise() {
			textComponent.text = text;
			boxCollider.size = ((RectTransform) transform).sizeDelta;
		}

		public void SetText(string text) {
			this.text = text;
			Initialise();
		}
	}
}