﻿namespace DigitalSalmon.C360.AnimatedComponents {
	public interface IAnimatedComponent {
		void OnAlphaChanged(float alpha);
	}
}