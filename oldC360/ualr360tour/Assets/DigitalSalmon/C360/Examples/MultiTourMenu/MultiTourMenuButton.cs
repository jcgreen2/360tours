﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace DigitalSalmon.C360 {
		[AddComponentMenu("Complete 360 Tour/Examples/Multi Tour Menu Button")]
		public class MultiTourMenuButton : Popup {
		//-----------------------------------------------------------------------------------------
		// Events:
		//-----------------------------------------------------------------------------------------

		public event EventHandler<MultiTourMenuButton> Selected;

		//-----------------------------------------------------------------------------------------
		// Inspector Variables:
		//-----------------------------------------------------------------------------------------

		[Header("Assignment")]
		[SerializeField]
		protected Image fill;

		[SerializeField]
		protected Text title;

		//-----------------------------------------------------------------------------------------
		// Public Properties:
		//-----------------------------------------------------------------------------------------

		public string Scene { get; private set; }

		//-----------------------------------------------------------------------------------------
		// Unity Lifecycle:
		//-----------------------------------------------------------------------------------------

		protected override void Awake() {
			base.Awake();
			SetupFill();
		}

		protected override void Start() {
			base.Start();
			StartCoroutine(SetupColliderDelayed());
		}

		//-----------------------------------------------------------------------------------------
		// Public Methods:
		//-----------------------------------------------------------------------------------------

		public void SetTargetScene(string target) {
			Scene = target;
			title.text = target.ToUpper();
		}

		public void SetFillColour(Color color) => fill.color = color;

		//-----------------------------------------------------------------------------------------
		// Protected Methods:
		//-----------------------------------------------------------------------------------------

		protected override void OnHoveredDeltaUpdate(float alpha) {
			base.OnHoveredDeltaUpdate(alpha);
			fill.fillAmount = alpha;
		}

		protected override void Trigger() {
			base.Trigger();
			Selected.InvokeSafe(this);
			SetHovered(false);
		}

		//-----------------------------------------------------------------------------------------
		// Private Methods:
		//-----------------------------------------------------------------------------------------

		private IEnumerator SetupColliderDelayed() {
			yield return null;
			SetupCollider();
		}

		private void SetupCollider() {
			BoxCollider col = gameObject.AddComponent<BoxCollider>();
			col.size = new Vector3(rectTransform.rect.width, rectTransform.rect.height, 1f);
		}

		private void SetupFill() {
			fill.sprite = Sprite.Create(Texture2D.whiteTexture, new Rect(0, 0, 1, 1), Vector2.one * 0.5f);
			fill.fillMethod = Image.FillMethod.Horizontal;
			fill.type = Image.Type.Filled;
			fill.fillOrigin = 0;
			fill.fillAmount = 0;
		}
	}
}