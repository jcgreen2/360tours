﻿using System.Collections.Generic;
using UnityEngine;

namespace DigitalSalmon.C360 {
	[AddComponentMenu("Complete 360 Tour/Core/Hotspot Reactor")]
	public class HotspotReactor : BaseBehaviour {
		//-----------------------------------------------------------------------------------------
		// Private Fields:
		//-----------------------------------------------------------------------------------------

		private readonly HashSet<Hotspot> activeHotspots = new HashSet<Hotspot>();

		//-----------------------------------------------------------------------------------------
		// Unity Lifecycle:
		//-----------------------------------------------------------------------------------------

		protected void OnEnable() { Complete360Tour.MediaSwitch += C360_MediaSwitch; }

		protected void OnDisable() { Complete360Tour.MediaSwitch -= C360_MediaSwitch; }

		//-----------------------------------------------------------------------------------------
		// Event Handlers:
		//-----------------------------------------------------------------------------------------

		protected void C360_MediaSwitch(MediaSwitchStates state, Node node) {
			if (state == MediaSwitchStates.BeforeSwitch) DestroyHotspots();
			if (state == MediaSwitchStates.Switch && node != null) ShowHotspots(node.AllHotspotElements);
		}

		private void Hotspot_Triggered(Hotspot hotspot) {
			if (hotspot == null || hotspot.Element == null) return;

			Complete360Tour.GoToMedia(hotspot.Element.TargetNode);
		}

		//-----------------------------------------------------------------------------------------
		// Private Methods:
		//-----------------------------------------------------------------------------------------

		private void DestroyHotspots() {
			foreach (Hotspot hotspot in activeHotspots) {
				if (hotspot != null) hotspot.HideAndDestroy();
			}
			activeHotspots.Clear();
		}

		private void ShowHotspots(IEnumerable<HotspotElement> hotspotElements) {
			if (hotspotElements == null) return;

			foreach (HotspotElement hotspotElement in hotspotElements) {
				Hotspot hotspot = Instantiate(hotspotElement.HotspotResource.Hotspot, transform).GetComponent<Hotspot>();

				hotspot.Construct(hotspotElement);
				hotspot.Triggered += Hotspot_Triggered;

				activeHotspots.Add(hotspot);
			}
		}
	}
}