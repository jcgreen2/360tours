﻿using System.Collections;
using UnityEngine;

namespace DigitalSalmon.C360 {
	public class TimelineVisibilityBehaviour : BaseBehaviour {
		//-----------------------------------------------------------------------------------------
		// Protected Fields:
		//-----------------------------------------------------------------------------------------

		protected IEnumerator timelineVisibility;

		//-----------------------------------------------------------------------------------------
		// Backing Fields:
		//-----------------------------------------------------------------------------------------

		private bool? isVisible;

		//-----------------------------------------------------------------------------------------
		// Protected Methods:
		//-----------------------------------------------------------------------------------------

		protected IEnumerator TimelineVisibilityCoroutine(ITimedElement timedElement) {
			bool alerted = false;
			while (true) {
				
				// Visibility from 'are we in the gap between entry and exit'.
				bool visible = IsAfterEntryTime(timedElement) && IsBeforeExitTime(timedElement);

				// Force visible if media duration is 0 (Usually default, unset value).
				if (Complete360Tour.ActiveNode is ITimelineNode timelineNode) {
					if (timelineNode.DurationMs == 0) visible = true;
				}
				// For visible is the current node is not a ITimelineNode.
				else {
					visible = true;
				}

				if (!isVisible.HasValue || isVisible != visible) {
					SetVisibility(visible);
				}
				if (Time.Seconds > timedElement.EntryTime / 1000 && !alerted) {
					alerted = true;
					CrossedEntryTime();
				}
				yield return null;
			}
		}

		protected virtual void SetVisibility(bool visible) {
			isVisible = visible;
		}

		protected virtual void CrossedEntryTime() { }
		private const int ERROR_MARGIN_MS = 100;

		private bool IsBeforeExitTime(ITimedElement timedElement) {
			if (Complete360Tour.ActiveNode is MediaNode mediaNode){
				if (mediaNode is ITimelineNode timelineNode) {
					double diff = (timelineNode.DurationMs - timedElement.ExitTime);
					if (diff < 0) diff = -1 * diff;
					if (diff < ERROR_MARGIN_MS) {
						return true;
					}
				}
			}
			return Time.Seconds <= (timedElement.ExitTime / 1000);
		}

		private bool IsAfterEntryTime(ITimedElement timedElement) {
			if (timedElement.EntryTime < ERROR_MARGIN_MS && Time.Seconds > 0) return true;
			return Time.Seconds >= (timedElement.EntryTime / 1000);
		}
	}
}