﻿using UnityEngine;

namespace DigitalSalmon.C360 {
	public enum MediaSwitchStates {
		BeforeSwitch,
		Switch,
		AfterSwitch
	}

	[AddComponentMenu("Complete 360 Tour/Complete 360 Tour")]
	public partial class Complete360Tour : ProtectedSingleton<Complete360Tour> {
		//-----------------------------------------------------------------------------------------
		// Events:
		//-----------------------------------------------------------------------------------------

		public static event EventHandler<MediaSwitchStates, Node> MediaSwitch;

		//-----------------------------------------------------------------------------------------
		// Inspector Variables:
		//-----------------------------------------------------------------------------------------

		[Header("Tour Settings")]
		[Header("Assignment")]
		[SerializeField]
		protected Tour tour;

		[SerializeField]
		[Tooltip("The name or uid of the node you would like to start your tour with.")]
		protected string entryId;

		[SerializeField]
		protected MediaTransition transition;

		[Header("Loading")]
		[SerializeField]
		[Tooltip("Should the tour begin as soon as the scene loads.")]
		protected bool autoBeginTour = true;

		[SerializeField]
		[Tooltip("Loads all image media into memory when the tour is loaded. Useful for URL loaded media.")]
		protected bool preloadImages = true;

	

		//-----------------------------------------------------------------------------------------
		// Public Properties:
		//-----------------------------------------------------------------------------------------

		/// <summary>
		/// The currently loaded Tour.
		/// </summary>
		public static Tour ActiveTour { get; private set; }

		/// <summary>
		/// The upcoming NodeData (Valid once a transition has started).
		/// </summary>
		public static Node NextNode { get; private set; }

		/// <summary>
		// The previous NodeData (Valid once a transition has started).
		/// </summary>
		public static Node PreviousNode { get; private set; }

		/// <summary>
		/// The current NodeData we are viewing.
		/// </summary>
		public static Node ActiveNode { get; private set; }

		//-----------------------------------------------------------------------------------------
		// Unity Lifecycle:
		//-----------------------------------------------------------------------------------------

		protected void Awake() {
			ActiveTour = tour;
		}

		protected void OnEnable() { transition.MediaSwitch += Transition_MediaSwitch; }

		protected void Start() {
			if (preloadImages && ActiveTour != null) {
				foreach (ImageMedia imageMedia in ActiveTour.ImageMedia) {
					imageMedia.LoadIfRequired();
				}
			}

			if (autoBeginTour) {
				if (ActiveTour == null) {
					Debug.LogWarning(Warnings.AUTOPLAY_MISSINGTOUR);
					return;
				}
				BeginTour();
			}
		}

		protected override void OnDisable() {
			base.OnDisable();
			transition.MediaSwitch -= Transition_MediaSwitch;
		}

		//-----------------------------------------------------------------------------------------
		// Event Handlers:
		//-----------------------------------------------------------------------------------------

		private void Transition_MediaSwitch(MediaSwitchStates state, Node node) {
			if (state == MediaSwitchStates.BeforeSwitch) {
				NextNode = node;
				PreviousNode = ActiveNode;
			}
			if (state == MediaSwitchStates.Switch) ActiveNode = node;
			MediaSwitch.InvokeSafe(state, node);
		}

		//-----------------------------------------------------------------------------------------
		// Public Methods:
		//-----------------------------------------------------------------------------------------

		/// <summary>
		/// Switches the MediaSwitch to the first piece of media.
		/// </summary>
		public void BeginTour() {
			Tour.Current = ActiveTour;
			Node firstNode = GetFirstMedia();
			GoToMedia(firstNode);
		}

		/// <summary>
		/// Instructs the MediaSwitch to move to a given NodeData.
		/// </summary>
		public static void GoToMedia(Node node) {
			if (node == null) {
				Debug.LogWarning(Warnings.GOTOMEDIA_MISSINGDATA);
				return;
			}
			if (Instance.transition == null) {
				Debug.LogWarning(Warnings.MISSING_TRANSITION);
				return;
			}

			if (Instance.transition.IsTransitioning) Instance.transition.Interrupt();
			Instance.transition.StartTransition(node);
		}

		/// <summary>
		/// Instructs the MediaSwitch to move to a given NodeData, located by node name.
		/// </summary>
		public static void GoToMedia(string nodeName) {
			Node node = GetNodeDataByName(nodeName);
			if (node == null) {
				Debug.LogWarning(Warnings.MISSING_NODEDATA_NAME(nodeName));
				return;
			}
			GoToMedia(node);
		}

		/// <summary>
		/// Returns the NodeData from the current tour, located by name.
		/// </summary>
		public static Node GetNodeDataByName(string name) {
			if (ActiveTour == null) return null;
			return ActiveTour.NodeCollection.FromName(name);
		}

		//-----------------------------------------------------------------------------------------
		// Private Methods:
		//-----------------------------------------------------------------------------------------

		private Node GetFirstMedia() {
			if (ActiveTour == null) {
				Debug.LogWarning(Warnings.MISSING_FIRSTMEDIA);
				return null;
			}

			Node namedNodeData = GetNodeDataByName(entryId);
			if (namedNodeData != null) return namedNodeData;
			return null;
		}
	}
}