﻿using DigitalSalmon;
using DigitalSalmon.C360;
using UnityEngine;

namespace DigitalSalmon.C360 {
	[AddComponentMenu("Complete 360 Tour/Transition/Media Transition")]
	public abstract class MediaTransition : BaseBehaviour {
		//-----------------------------------------------------------------------------------------
		// Events:
		//-----------------------------------------------------------------------------------------

		public event EventHandler<MediaSwitchStates, Node> MediaSwitch;

		//-----------------------------------------------------------------------------------------
		// Public Properties:
		//-----------------------------------------------------------------------------------------

		public bool IsTransitioning { get; protected set; }
		protected bool IsLoading { get; private set; }
		//-----------------------------------------------------------------------------------------
		// Public Methods:
		//-----------------------------------------------------------------------------------------

		public abstract void StartTransition(Node node);
		public virtual void Interrupt() { }

		public void SetIsLoading(bool isLoading) { IsLoading = isLoading; }

		//-----------------------------------------------------------------------------------------
		// Protected Methods:
		//-----------------------------------------------------------------------------------------

		protected void InvokeMediaSwitch(MediaSwitchStates state, Node node) { MediaSwitch.InvokeSafe(state, node); }
	}
}