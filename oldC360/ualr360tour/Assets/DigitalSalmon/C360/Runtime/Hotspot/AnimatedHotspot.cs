﻿using System.Collections;
using UnityEngine;
using UnityTime = UnityEngine.Time;

namespace DigitalSalmon.C360 {
	[AddComponentMenu("Complete 360 Tour/Hotspot/Animated Hotspot")]
	public class AnimatedHotspot : Hotspot {
		//-----------------------------------------------------------------------------------------
		// Unity Lifecycle:
		//-----------------------------------------------------------------------------------------

		[Header("Hotspot")]
		[SerializeField]
		protected float triggerDuration = 1.5f;

		//-----------------------------------------------------------------------------------------
		// Private Fields:
		//-----------------------------------------------------------------------------------------

		private float hoveredAlpha;
		private bool triggerLocked;

		//-----------------------------------------------------------------------------------------
		// Unity Lifecycle:
		//-----------------------------------------------------------------------------------------

		protected override void OnEnable() {
			base.OnEnable();
			StartCoroutine(HoverCoroutine());
		}

		protected override void OnDisable() {
			base.OnDisable();
			StopAllCoroutines();
		}

		//-----------------------------------------------------------------------------------------
		// Protected Methods:
		//-----------------------------------------------------------------------------------------

		public override void Construct(HotspotElement element) {
			base.Construct(element);
			OnHoveredDeltaUpdate(0);
		}

		protected override void VisiblityChanged(bool visible) {
			base.VisiblityChanged(visible);
			const string VISIBILITY_PARAM = "Visible";
			if (animator != null) animator.SetBool(VISIBILITY_PARAM, visible);
		}

		protected override void HoveredChanged(bool hovered) {
			base.HoveredChanged(hovered);
			triggerLocked = false;
		}

		protected virtual void OnHoveredDeltaUpdate(float alpha) {
			if (hotspotRenderer != null && Application.isPlaying) hotspotRenderer.material.SetFloat("_FillValue", alpha);
		}

		//-----------------------------------------------------------------------------------------
		// Private Methods:
		//-----------------------------------------------------------------------------------------

		private IEnumerator HoverCoroutine() {
			while (true) {
				bool alphaChanged = false;

				if (IsHovered && hoveredAlpha < 1) {
					hoveredAlpha += UnityTime.deltaTime / triggerDuration;
					if (hoveredAlpha > 1) hoveredAlpha = 1;
					alphaChanged = true;
				}
				if (!IsHovered && hoveredAlpha > 0) {
					hoveredAlpha -= UnityTime.deltaTime / triggerDuration;
					if (hoveredAlpha < 0) hoveredAlpha = 0;
					alphaChanged = true;
				}

				if (IsHovered && hoveredAlpha == 1 && !triggerLocked) {
					Trigger();
					triggerLocked = true;
				}

				if (alphaChanged) OnHoveredDeltaUpdate(hoveredAlpha);

				yield return null;
			}
		}
	}
}