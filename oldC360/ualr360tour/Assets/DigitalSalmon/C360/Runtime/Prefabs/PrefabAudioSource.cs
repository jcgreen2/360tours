﻿using UnityEngine;

namespace DigitalSalmon.C360 {
	[AddComponentMenu("Complete 360 Tour/Examples/Prefab Audio Source")]
	public class PrefabAudioSource : TimelineVisibilityBehaviour, IMappedPrefab {
		//-----------------------------------------------------------------------------------------
		// Inspector Variables:
		//-----------------------------------------------------------------------------------------

		[SerializeField]
		protected bool loop;

		//-----------------------------------------------------------------------------------------
		// Private Fields:
		//-----------------------------------------------------------------------------------------

		private AudioSource audioSource;

		//-----------------------------------------------------------------------------------------
		// Private Properties:
		//-----------------------------------------------------------------------------------------

		private PrefabElement Element { get; set; }

		//-----------------------------------------------------------------------------------------
		// Unity Lifecycle:
		//-----------------------------------------------------------------------------------------

		protected void Awake() { audioSource = GetComponent<AudioSource>(); }

		protected void Start() { audioSource.loop = loop; }

		//-----------------------------------------------------------------------------------------
		// Interface Methods:
		//-----------------------------------------------------------------------------------------

		void IMappedPrefab.UpdateState(MediaSwitchStates state) { }

		void IMappedPrefab.UpdateData(PrefabElement element, Node node) {
			Element = element;
			if (timelineVisibility != null) StopCoroutine(timelineVisibility);

			if (Element == null) return;
			timelineVisibility = TimelineVisibilityCoroutine(Element);
			StartCoroutine(timelineVisibility);
		}

		//-----------------------------------------------------------------------------------------
		// Protected Methods:
		//-----------------------------------------------------------------------------------------

		protected override void SetVisibility(bool visible) {
			base.SetVisibility(visible);
			if (visible) {
				audioSource.Play();
			}
			else {
				audioSource.Stop();
			}
		}
	}
}