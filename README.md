# 360tours

Repository for 360 Tour Applications.

## Description:
This is a repository containing 360 video tours done for the EAC. This project uses the Complete 360 unity package.

## Features:
Author: Julia Green

Unity version used: 2019.3.5f1