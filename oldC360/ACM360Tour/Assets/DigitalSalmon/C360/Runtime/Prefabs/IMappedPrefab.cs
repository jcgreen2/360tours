﻿namespace DigitalSalmon.C360 {
	public interface IMappedPrefab {
		void UpdateState(MediaSwitchStates state);
		void UpdateData(PrefabElement element, Node node);
	}
}