﻿using UnityEngine;

namespace DigitalSalmon.C360 {
	[AddComponentMenu("Complete 360 Tour/Core/Image Reactor")]
	public class ImageReactor : MediaReactor, ITimeController {
		//-----------------------------------------------------------------------------------------
		// Private Fields:
		//-----------------------------------------------------------------------------------------

		private MediaClock clock;

		//-----------------------------------------------------------------------------------------
		// Interface Fields:
		//-----------------------------------------------------------------------------------------

		int ITimeController.Priority => 0;
		double ITimeController.Time => clock.TimeMs;

		//-----------------------------------------------------------------------------------------
		// Event Handlers:
		//-----------------------------------------------------------------------------------------

		protected override void C360_MediaSwitch(TransitionState state, Node node) {
			if (state == TransitionState.Switch) SwitchMedia(node);
		}

		//-----------------------------------------------------------------------------------------
		// Protected Methods:
		//-----------------------------------------------------------------------------------------

		protected void SwitchMedia(Node node) {
			if (!(node is ImageNode imageNode)) return;

			if (imageNode.Loop) clock = MediaClock.Looped(imageNode.DurationMs);
			else clock = MediaClock.Limited(imageNode.DurationMs);

			Time.RegisterController(this);

			Surface.SetTexture(imageNode.Thumbnail);
			Surface.SetStereoscopic(imageNode.IsStereoscopic);
			Surface.SetProjection(imageNode.Projection);
			Surface.SetYFlip(false);
		}
	}
}