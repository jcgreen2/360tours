﻿using TMPro;
using UnityEngine;

namespace DigitalSalmon.C360 {
	public class LabelledAnimatedHotspot : AnimatedHotspot {
		//-----------------------------------------------------------------------------------------
		// Serialized Fields:
		//-----------------------------------------------------------------------------------------

		[SerializeField]
		protected TMP_Text textComponent;

		//-----------------------------------------------------------------------------------------
		// Public Methods:
		//-----------------------------------------------------------------------------------------

		public override void Construct(HotspotElement element) {
			base.Construct(element);

			if (textComponent != null) {
				textComponent.text = element?.TargetNode?.Name;
			}
		}
	}
}