﻿using UnityEngine;

namespace DigitalSalmon.C360 {
	[AddComponentMenu("Complete 360 Tour/Timeline/Hotspot Timeline Visibility")]
	public class HotspotTimelineVisibilityBehaviour : TimelineVisibilityBehaviour {
		//-----------------------------------------------------------------------------------------
		// Serialized Fields:
		//-----------------------------------------------------------------------------------------

		[SerializeField]
		protected bool verbose;
		//-----------------------------------------------------------------------------------------
		// Private Fields:
		//-----------------------------------------------------------------------------------------

		private Hotspot hotspot;

		//-----------------------------------------------------------------------------------------
		// Unity Lifecycle:
		//-----------------------------------------------------------------------------------------

		protected void Awake() {
			hotspot = GetComponent<Hotspot>();
			hotspot.Constructed += Hotspot_Constructed;
		}

		//-----------------------------------------------------------------------------------------
		// Event Handlers:
		//-----------------------------------------------------------------------------------------

		private void Hotspot_Constructed() {
			sequence.Cancel();
			if (!hotspot.IsValid) return;
			sequence.Coroutine(TimelineVisibilityCoroutine(hotspot.Element));
		}

		//-----------------------------------------------------------------------------------------
		// Protected Methods:
		//-----------------------------------------------------------------------------------------

		protected override void SetVisibility(bool visible) {
			base.SetVisibility(visible);
			if (hotspot != null) hotspot.SetVisible(visible);
		}

		protected override void CrossedEntryTime() {
			base.CrossedEntryTime();
			if (hotspot.Element == null) return;
			if (((ITimedElement) hotspot.Element).EntryTime == 0) return;
			if (((ITimedElement) hotspot.Element).EntryTime == ((ITimedElement) hotspot.Element).ExitTime) hotspot.ForceTrigger();
		}
	}
}